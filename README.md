# Databook

E-book collection manager

## Getting started

This program is designed to search books by author. The directory structure will be **../mylibary/autor_X** where "mylibary" is the directory that contains all the authors subdirectories (autor_X) one for each.

The first time you run the program, it will create a directory called **/home/[USER]/.databook**, then, inside this directory it will create and then update the **config.json** file.

## Add your e-books directory

- Choose the Library directory
- Refresh the data
- Select an author
- Reset the data

## Authors and acknowledgment
Thanks to Benoît Minisini for developing the [Gambas IDE](https://gitlab.com/gambas/gambas).
Thanks to all [gambas-es.org](https://gambas-es.org/) forum colleagues especially Vuott, Tercoide and Shordi.


## License
This is Free software under GPL v3. This program is supplied as is with no warranty of any kind.
I hope this program will be useful for you and that you feel free to use it and comment any suggestions.


## Project status
This project is at a very early stage so you will find some bugs and things that are not available please report any bugs or improvements to us.

[Contact Us](mailto:info@belmotek.net)
